package de.oszimt.fos42.homecontrolfx.api;

public class TestHomeControlRest {

	public static void main(String[] args) {
		// New Home Control Server
		HomeControlRest myRestServer = new HomeControlRest();
		// Test with localhost
		myRestServer.setServerAdress("http://localhost:8080");
		// Generate the auth key
		String myKey = myRestServer.generateAuthKey("Hallo", "Welt!");
		System.out.println("Received token: " + myKey);
		myRestServer.setAuthKey(myKey);
		if (!myRestServer.checkAuthKey())
		{
			System.out.println("Login error - Token:" + myKey);
			return;
		}
		System.out.println("Login ok - User: " + myRestServer.getUserName());
		// Get temp
		double curTemp = myRestServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");			
		// Get relais state
		boolean curState = myRestServer.getLightState();
		System.out.println("Relais is " + (curState ? "on" : "off") + "!");
		System.out.println("Relais changed to " + (myRestServer.switchLightState() ? "on" : "off") + "!");
	}

}
