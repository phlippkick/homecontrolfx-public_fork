/**
 * 
 */
package de.oszimt.fos42.homecontrolfx.api;

/**
 * Simulates an HomeControlFx server for local development. Uses console output 
 * to show the server state, authentication is implemented but not required.
 * 
 * @author M. Schleyer
 *
 */
public class HomeControlSim implements IHomeControl {

	private String authKey;
	private boolean lightState = false;
	private double temperature = 23.0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#setAuthKey(java.lang.String)
	 */
	@Override
	public void setAuthKey(String key) {
		this.authKey = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#generateAuthKey(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public String generateAuthKey(String username, String password) {
		System.out.println("[INFO] Key is generated: " + Hash.sha256(username + ":" + password) );
		return Hash.sha256(username + ":" + password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#setLightState(boolean)
	 */
	@Override
	public void setLightState(boolean state) {
		System.out.println("[INFO] Light switched to " + (state ? "on" : "off"));
		this.lightState = state;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getLightState()
	 */
	@Override
	public boolean getLightState() {
		System.out.println("[INFO] Light state requested, state is " + (this.lightState ? "on" : "off"));
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#switchLightState()
	 */
	@Override
	public boolean switchLightState() {
		// TODO Auto-generated method stub
		this.lightState = !this.lightState;
		System.out.println("[INFO] Light state switched, state is " + (this.lightState ? "on" : "off"));
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getTemperature()
	 */
	@Override
	public double getTemperature() {
		this.temperature = this.temperature + (Math.random() - 0.5);
		System.out.println("[INFO] Temperature requested, state is " + this.temperature + "°C");
		return this.temperature;
	}

	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#checkAuthKey()
	 */
	@Override
	public boolean checkAuthKey() {
		// Check the auth key :-)
		boolean keyCheck = this.authKey.equals(generateAuthKey("Hallo", "Welt!"));
		System.out.println("[INFO] Key is checked, is  " +  (keyCheck ? "ok" : "invalid") + "!");
		return keyCheck;
	}

	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#getUserLogin()
	 */
	@Override
	public String getUserLogin() {
		return "Hallo";
	}

	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#getUserName()
	 */
	@Override
	public String getUserName() {
		return "Test-Benutzer";
	}

}
