package de.oszimt.fos42.homecontrolfx.gui;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import de.oszimt.fos42.homecontrolfx.api.UserLogin;

/**
 * <p>
 * Vorlage für den Controller. In dieser Klasse werden alle Events definiert.
 * Events sind die Aktionen, die vom Benutzer ausgelöst werden können. Dazu
 * gehört zum Beispiel der Klick auf einen Button.
 * </p>
 * 
 * @author Martin Schleyer
 *
 */
public class LoginWindowController {

	/**
	 * Erstellt ein Objekt der UserLogin-Klasse mit Test-Benutzerdaten.
	 * 
	 * @see de.oszimt.fos42.homecontrolfx.api.UserLogin
	 */
	private UserLogin userLogin = new UserLogin("Hallo", "Welt!");
	
	@FXML
    private TextField tfdUserName;

    @FXML
    private PasswordField pfdPassword;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnLogin;
    
    private Pane pnMain;
    
    private Stage mainStage;

    @FXML
    void btnCancelAction(ActionEvent event) 
    {
    	mainStage = (Stage) btnCancel.getScene().getWindow();
    	mainStage.close();
    }

    @FXML
    void btnLoginAction(ActionEvent event) throws Exception 
    {
    	if (userLogin.checkLogin(tfdUserName.getText(), pfdPassword.getText()))
    	{
    		Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
    		mainStage = new Stage();
    		mainStage.setScene(new Scene(root));
    		mainStage.show();
    		mainStage = (Stage) btnCancel.getScene().getWindow();
    		mainStage.close();
    	}
    	else if (tfdUserName.getText().equals("") && pfdPassword.getText().equals(""))
    	{
    		new Alert(AlertType.WARNING, "No typing, no login!").show();
    	}
    	else
    	{
    		new Alert(AlertType.ERROR, "WROOOOONG!").show();
    	}
    }
    
    
	/**
	 * Hier kommen die weiteren Attribute des Controllers hin. Sie können sich
	 * ein Beispiel dazu direkt im SceneBuilder anschauen.
	 * 
	 * Wählen Sie dazu "View" &gt;&gt; "Show Sample Controller Skeleton" im Menü
	 * aus, während Sie die Datei <i>LoginWindow.fxml</i> bearbeiten.
	 */

	// Attribute...

	/**
	 * Anschließend folgen die einzelnen Events, die Sie im SceneBuilder
	 * definiert haben. Beispiel:
	 * 
	 *  protected void btnHalloWeltClick(){
	 *       System.out.println("Hallo Welt!"); }
	 */

	// Methoden ...
}
