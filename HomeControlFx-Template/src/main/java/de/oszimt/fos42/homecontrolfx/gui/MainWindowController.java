package de.oszimt.fos42.homecontrolfx.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;



public class MainWindowController 
{
 
	
	@FXML
	private Button btnLogout;

	@FXML
	private Button btnClose;
	
	@FXML
	private Button btnRefresh;
	
    @FXML
    private TextField tfdTemperature;

    @FXML
    private CheckBox ckbLicht;
    
    
    @FXML
    private ImageView picLight;
    
	private Stage mainStage;
	
	private ProgressBar prgbTemperature;
	
	@FXML
	private String fahrenheit;
	
	@FXML
	private String celsius0;
	
	
	
	@FXML
	void btnCloseAction(ActionEvent event)  
	{
		mainStage = (Stage) btnLogout.getScene().getWindow();
		mainStage.close();
	}

	@FXML
	void btnLogoutAction(ActionEvent event) throws Exception
	{
		Parent root = FXMLLoader.load(getClass().getResource("LoginWindow.fxml"));
		mainStage = new Stage();
		mainStage.setScene(new Scene(root));
		mainStage.show();
		mainStage = (Stage) btnLogout.getScene().getWindow();
		mainStage.close();
	}

    @FXML
    void btnRefreshAction(ActionEvent event) 
    {
    	this.tfdTemperature.setText(String.valueOf(LoginWindow.server.getTemperature()));
    }
    
    
    //Temperatur
    
    //Celsius
    @FXML
    void btnCelsius(ActionEvent event) 
    {
    	this.tfdTemperature.setText(String.valueOf(LoginWindow.server.getTemperature()));
//    	Fahrenheit = (9/5) * Celsius + 32

    }
    
    //Fahrenheit
    @FXML
    void btnFahrenheit(ActionEvent event) 
    {
    	
    	
    	double celsius = LoginWindow.server.getTemperature();
		double tmp_fahrenheit = (9/5) * celsius + 32;
		tfdTemperature.setText(tmp_fahrenheit +"");
//    	this.tfdTemperature.setPromptText(fahrenheit);
    }

    @FXML
    void ckbLichtClick(ActionEvent event) {
  
    	
    	if(ckbLicht.isSelected()){
    	
    		LoginWindow.server.setLightState(true);
    		Image image = new Image(getClass().getResource("light-on.png").toString());
        	picLight.setImage(image);
        	}
        	
    	else{
    		LoginWindow.server.setLightState(false);
    		Image image = new Image(getClass().getResource("light-off.png").toString());
        	picLight.setImage(image);
        	}
        	
    }
   }
