/**
 * 
 */
package de.oszimt.fos42.homecontrolfx.api;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Connects to an HomeControlFx server using an HTTP REST-API. The server normally
 * expects authentication, so make sure to set a proper AUTH key.
 * 
 * @author M. Schleyer
 *
 */
public class HomeControlRest implements IHomeControl {

	private String serverAdress = "homecontrol.local:8080";

	private String authKey;
	private String userName;
	private String userLogin;

	private boolean lightState = false;
	private double temperature = 23.0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#setAuthKey(java.lang.String)
	 */
	@Override
	public void setAuthKey(String key) {
		this.authKey = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#generateAuthKey(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public String generateAuthKey(String username, String password) {
		// Request an auth key
		String newKey = "";
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAdress + "/api/token/{user}/{pw}")
					.routeParam("user", username).routeParam("pw", password).asJson();

			switch (jsonResponse.getStatus()) {
			case 200:
				newKey = jsonResponse.getBody().getObject().getString("token");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.format("Failed to connect to server %s!%n", serverAdress);
		}
		return newKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#setLightState(boolean)
	 */
	@Override
	public void setLightState(boolean state) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAdress + "/relais/set/{state}")
					.routeParam("state", (state ? "on" : "off")).queryString("auth", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightState = jsonResponse.getBody().getObject().getString("relais").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			System.out.format("Failed to connect to server %s!%n", serverAdress);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getLightState()
	 */
	@Override
	public boolean getLightState() {
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAdress + "/relais/get")
					.queryString("auth", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightState = jsonResponse.getBody().getObject().getString("relais").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			System.out.format("Failed to connect to server %s!%n", serverAdress);
		}
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#switchLightState()
	 */
	@Override
	public boolean switchLightState() {
		this.setLightState(!this.getLightState());
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getTemperature()
	 */
	@Override
	public double getTemperature() {
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAdress + "/temp/get")
					.queryString("auth", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.temperature = jsonResponse.getBody().getObject().getDouble("temperature");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			System.out.format("Failed to connect to server %s!%n", serverAdress);
		}
		return this.temperature;
	}

	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#checkAuthKey()
	 */
	@Override
	public boolean checkAuthKey() {
		// Check the auth key
		boolean keyCheck = false;
		// Send the query
		try {		
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAdress + "/api/check")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.userLogin = jsonResponse.getBody().getObject().getString("user");
				this.userName = jsonResponse.getBody().getObject().getString("name");
				keyCheck = true;
			}
		} catch (UnirestException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.format("Failed to connect to server %s!%n", serverAdress);
		}
		return keyCheck;
	}

	/**
	 * reads the server adress to which the REST-API should connect to
	 * 
	 * @return
	 */
	public String getServerAdress() {
		return serverAdress;
	}

	/**
	 * Sets the URL to the HomeControlFx server, e.g. http://homecontrolfx.local:8080/
	 * 
	 * @param serverAdress
	 */
	public void setServerAdress(String serverAdress) {
		this.serverAdress = serverAdress;
	}


	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#getUserName()
	 */
	@Override
	public String getUserName() {
		checkAuthKey();
		return userName;
	}

	/* (non-Javadoc)
	 * @see de.oszimt.fos42.homecontrolfx.api.IHomeControl#getUserLogin()
	 */
	@Override
	public String getUserLogin() {
		checkAuthKey();
		return userLogin;
	}
}
